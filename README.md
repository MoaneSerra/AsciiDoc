# AsciiDoc : Manuel de référence sur `asciidoc`

[[__TOC__]]

## Présentation

Dépôt dont le but est de créer un manuel de référence sur un langage de balisage plus complet que `Markdown` peut-être plus abordable que \LaTeX : `asciidoc`.

L'objectif ici sera de créer d'abord en Français une documentation sur ce langage de balisage `asciidoc` puis ensuite d'en créer des versions multilingues.

## Structure

Le dépôt contient :

+ un dossier racine avec différentes explications
+ un dosier `documentations` avec des fac-similés ou des *pdf* de documentations récupérées sur internet temporairement pour servir de bibliographie, puis effacées une fois le projet suffisament mûr.
+ un dossier par langue de type "LANGUE_langue" contenant aussi les images dans un premier temps.